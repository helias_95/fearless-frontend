// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/states/';

//     const response = await fetch(url);

//     if (response.ok) {
//         const data = await response.json();
//         console.log(data);
//         const selectTag = document.getElementById('state');
//         for (let state of data.states) {
//           // Create an 'option' element
//           const option = document.createElement("option");
//           // Set the '.value' property of the option element to the
//           // state's abbreviation
//           option.value = state.abbreviation;

//           // Set the '.innerHTML' property of the option element to
//           // the state's name
//           option.innerHTML = state.name; // this allows you to dictate what you want the html to show in this tag.
//           // by default, this would be a dictionary, but so we declare state.name to be the only thing that gets shown within each
//           //option of the drop down.
//           // Append the option element as a child of the select tag
//           selectTag.append(option); // .append() add text to an element, so we are adding option to the
//          // prevent the default submit event in the form, bc we want the
//           const formTag = document.getElementById('create-location-form');
//           formTag.addEventListener('submit', async event => {
//             event.preventDefault();
//             // convert the form data into json:
//             const formData = new FormData(formTag); //create a new form data
//             const json = JSON.stringify(Object.fromEntries(formData)); // translates it into json--
//             console.log(json); // will log an empty object unless you have a name attribute in your tag in the html


//             // now we need to send the data to the server after creating the form data:
//             const locationUrl = 'http://localhost:8000/api/locations/';
//             const fetchConfig = {
//                 method: "post",
//                 body: json,
//                 headers: {
//                     'Content-Type': 'application/json',
//                 },
//             };
//             const response = await fetch (locationUrl, fetchConfig);
//             if (response.ok) {
//                 formTag.reset();
//                 const newLocation = await response.json();
//                 console.log(newLocation);
//             }
//           });
//         }
//     }
//   });

//we need to access the location;s state information so that we can include it
// in the State drop dow n of our create location form.```
/*
We first had to create a list view fucntion for the states. In order to be able to have access
to the list of states.
we had to create an api request of said state list in insomnia.
*/

window.addEventListener("DOMContentLoaded", async () => {
	// fetch a url api request and set the response to a variable
    // we are accessing the list of states from the url, and saving the response in a var
    const url = "http://localhost:8000/api/states/";
	const response = await fetch(url); // we must use await and fetch bc it is an asynchronous function

	if (response.ok) { // if the response from fetching the url is good:
		const data = await response.json(); // put that response into json encoded date. (makes a json object/dict)
		// console.log(data);
        //
        //set a variable for the select tag that is the drop down of states in our form
		const selectTag = document.getElementById("state");
        // now we will iterate through the list of states and put it in a new element "option"
		for (let state of data.states) {
			// Create an 'option' element
			const option = document.createElement("option");
			// Set the '.value' property of the option element to the state's abbreviation
			option.value = state.abbreviation;
			// Set the '.innerHTML' property of the option element to the state's name
			option.innerHTML = state.name;
			// Append the option element as a child of the select tag
			selectTag.appendChild(option);
		}

		const formTag = document.getElementById("create-location-form");
		formTag.addEventListener("submit", async (event) => {
			event.preventDefault();
			// console.log("need to submit the form data");

			const formData = new FormData(formTag);
			const json = JSON.stringify(Object.fromEntries(formData));
			// console.log(json);
			const locationUrl = "http://localhost:8000/api/locations/";
			const fetchConfig = {
				method: "post",
				body: json,
				headers: {
					"Content-Type": "application/json",
				},
			};
			const response = await fetch(locationUrl, fetchConfig);
			if (response.ok) {
				formTag.reset(); //resets the form after submission event
				const newLocation = await response.json();
                console.log(newLocation)
			}
		});
	}
});
