window.addEventListener("DOMContentLoaded", async () => {

	const url = "http://localhost:8000/api/locations/";
	const response = await fetch(url); // we must use await and fetch bc it is an asynchronous function

	if (response.ok) { // if the response from fetching the url is good:
		const data = await response.json(); // put that response into json encoded date. (makes a json object/dict)
		console.log(data);
		const selectTag = document.getElementById("locations");
		for (let location of data.locations) {
			// Create an 'option' element
			const option = document.createElement("option");
			option.value = location.name;
			option.innerHTML = location.name;
			// selectTag.append(option);
		}
    }
});
