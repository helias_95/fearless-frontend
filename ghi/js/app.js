function createCard(name, description, pictureUrl, starts, ends, location) {
    // for column in columns do this:
    // there are 3 columns, if the column
    const startDate = new Date(starts).toLocaleDateString()
    const endDate = new Date(ends).toLocaleDateString()

    return `
    <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer text-body-secondary">
          ${startDate} - ${endDate}
        </div>
          </div>
        </div>
      </div>
    `;
  }


window.addEventListener("DOMContentLoaded", async () => {
	const url = "http://localhost:8000/api/conferences/";

	try {
		const response = await fetch(url);

		if (!response.ok) {
			 // create a variable to store the bootstrap
             const errorMessage = `
             <div class="alert alert-danger" role="alert">
                 Error getting the data from the API
             </div>
         `;
         // create a variable to store an empty div within
         const errorContainer = document.createElement("div");
         // store the bootstrap div within the empty div you created
         errorContainer.innerHTML = errorMessage;

         // create a variable to find the first child element of body
         const firstChild = document.body.firstChild;
         // insert your div as the first child element of the body to appear at the top of the page
         document.body.insertBefore(errorContainer, firstChild);
		} else {
			const data = await response.json();
			// console.log(data);

			// const conference = data.conferences[0];
			// const nameTag = document.querySelector(".card-title");
			// nameTag.innerHTML = conference.name;

			// const detailUrl = `http://localhost:8000${conference.href}`;
			// const detailResponse = await fetch(detailUrl);
			// if (detailResponse.ok) {
			// 	const details = await detailResponse.json();

			// 	const description = document.querySelector(".card-text");
			// 	description.innerHTML = details.conference.description;

			// 	const imageTag = document.querySelector(".card-img-top");
			// 	imageTag.src = details.conference.location.picture_url;

                let counter = 0;
                // for (let conference of data.conferences)
                for (let i = 0; i < data.conferences.length; i++) {
                    const detailUrl = `http://localhost:8000${data.conferences[i].href}`;
                    const detailResponse = await fetch(detailUrl);
                    if (detailResponse.ok) {
                      const details = await detailResponse.json();
                      const name = details.conference.name;
                      const description = details.conference.description;
                      const pictureUrl = details.conference.location.picture_url;
                      const location = details.conference.location.name

                      const starts = details.conference.starts;
                      const ends = details.conference.ends;

                      const html = createCard(name, description, pictureUrl, starts, ends, location);

                      const columns = document.querySelectorAll('.col'); //columns is an array (a Nodelist) that lists where all the 'col' tags appear in the index
                      const column = columns[counter]; //column is an object; grabs the location we will put the card into from the querySelector ^ using the counter as the index for the column we will use.
                      column.innerHTML += html;  // puts the card in the column that we have selected
                      counter++ //add 1 to 'counter' to go to the next column
                      if(counter === 3) { // there are only three columns so we want to go back to the first one once we reach the third one
                        counter = 0;
                      }
    			}
		    }
	    }
    }   catch(e) {
		// Figure out what to do if an error is raised
		console.error(e);
	}
});
